// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Patterns/AbstarctFactory/LootStructures.h"
#include "Loot.generated.h"

UCLASS()
class PATTERNS_API ALoot : public AActor
{
	GENERATED_BODY()

public:
	/** Puts this item to our inventory */
	UFUNCTION(BlueprintCallable)
		virtual void StoreItem() PURE_VIRTUAL(AItem::StoreItem, );

	/** Uses this item for something */
	UFUNCTION(BlueprintCallable)
		virtual void UseItem() PURE_VIRTUAL(AItem::UseItem, );

	/** Each descendant knows, what info it awaits and how to use it */
	virtual void SetupWithInfo(FBaseInfo* InfoPtr){};

private:
	/** Mesh of an actual item descendant */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Meshes", meta = (AllowPrivateAccess = true))
		class USkeletalMesh* ItemMesh;
};
